package proxy.dynamicproxy.jdk

import proxy.staticproxy.WorkerInterface

object Start {
    @JvmStatic
    fun main(args: Array<String>) {
        val client = Client()
        val workerProxy = client.getDynamicProxy()
        workerProxy.work()
        workerProxy.happy()
        workerProxy.relax()
    }
}