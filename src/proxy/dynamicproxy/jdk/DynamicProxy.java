package proxy.dynamicproxy.jdk;

import proxy.staticproxy.WorkerInterface;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DynamicProxy implements InvocationHandler {

    private WorkerInterface worker;

    public DynamicProxy(WorkerInterface workerInterface) {
        this.worker = workerInterface;
    }

    public void before() {
        System.out.println("开始前干点什么吧");
    }

    public void after() {
        System.out.println("结束后干点什么吧");
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object result = method.invoke(worker, args);
        after();
        return result;
    }
}
