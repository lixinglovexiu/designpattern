package proxy.dynamicproxy.jdk

import proxy.staticproxy.Worker
import proxy.staticproxy.WorkerInterface

import java.lang.reflect.Proxy

class Client {

    fun getDynamicProxy(): WorkerInterface{
        val worker = Worker()
        val dynamicProxy = DynamicProxy(worker)
        return Proxy.newProxyInstance(dynamicProxy.javaClass.classLoader, worker.javaClass.interfaces, dynamicProxy) as WorkerInterface
    }
}
