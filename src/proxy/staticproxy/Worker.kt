package proxy.staticproxy

class Worker: WorkerInterface {
    override fun happy() {
        println("工人找乐子")
    }

    override fun relax() {
        println("工人休息")
    }

    override fun work() {
        println("工人干活")
    }


}