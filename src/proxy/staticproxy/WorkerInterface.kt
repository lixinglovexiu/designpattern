package proxy.staticproxy

interface WorkerInterface {
    fun work()

    fun relax()

    fun happy()
}