package proxy.staticproxy

class StaticProxy(private val worker: WorkerInterface) : WorkerInterface {

    private fun before() {
        println("开始前干点什么吧")
    }

    private fun after() {
        println("结束后干点什么吧")
    }

    override fun relax() {
        before()
        worker.relax()
        after()
    }

    override fun happy() {
        before()
        worker.happy()
        after()
    }

    override fun work() {
        before()
        worker.work()
        after()
    }

}