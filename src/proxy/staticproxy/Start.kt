package proxy.staticproxy

object Start {
    @JvmStatic
    fun main(args: Array<String>) {
        val worker = Worker()
        val proxy = StaticProxy(worker)
        proxy.work()
        proxy.happy()
        proxy.relax()
    }
}